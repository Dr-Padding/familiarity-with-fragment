package com.example.familiaritywithfragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.familiaritywithfragment.databinding.Fragment1Binding
import com.example.familiaritywithfragment.databinding.Fragment2Binding
import com.example.familiaritywithfragment.databinding.Fragment3Binding


class Fragment3 : Fragment(R.layout.fragment_1) {

    private var binding: Fragment3Binding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = Fragment3Binding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {
        binding?.apply {
            //handle onClickListeners if needed
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}