package com.example.familiaritywithfragment

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import com.example.familiaritywithfragment.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity(), Fragment1.FragmentListener {

    private lateinit var binding: ActivityMainBinding
    private var isFragment2Displayed = true
    private var color2 = 0
    private var color3 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(binding.fragmentContainerOne.id, Fragment1())
                add(binding.fragmentContainerTwo.id, Fragment2())
                add(binding.fragmentContainerThree.id, Fragment3())
            }
        } else {
            isFragment2Displayed = savedInstanceState.getBoolean("isFragment2Displayed")
            color2 = savedInstanceState.getInt("color2")
            color3 = savedInstanceState.getInt("color3")
            binding.fragmentContainerTwo.setBackgroundColor(color2)
            binding.fragmentContainerThree.setBackgroundColor(color3)
            if (!isFragment2Displayed) {
                supportFragmentManager.commit {
                    replace(binding.fragmentContainerTwo.id, Fragment3())
                    replace(binding.fragmentContainerThree.id, Fragment2())
                }
            }
        }
    }

    override fun onSwapFragments() {
        if (isFragment2Displayed) {
            supportFragmentManager.commit {
                replace(binding.fragmentContainerTwo.id, Fragment3())
                replace(binding.fragmentContainerThree.id, Fragment2())
            }
        } else {
            supportFragmentManager.commit {
                replace(binding.fragmentContainerTwo.id, Fragment2())
                replace(binding.fragmentContainerThree.id, Fragment3())
            }
        }
        isFragment2Displayed = !isFragment2Displayed
    }

    override fun onChangeBackgroundColors() {
        color2 = getRandomColor()
        color3 = getRandomColor()
        binding.apply {
            fragmentContainerTwo.setBackgroundColor(color2)
            fragmentContainerThree.setBackgroundColor(color3)
        }
    }

    private fun getRandomColor(): Int {
        return Color.rgb(Random().nextInt(256), Random().nextInt(256), Random().nextInt(256))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("isFragment2Displayed", isFragment2Displayed)
        outState.putInt("color2", color2)
        outState.putInt("color3", color3)
    }

}