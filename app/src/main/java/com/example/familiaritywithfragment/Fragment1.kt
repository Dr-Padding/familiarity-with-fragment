package com.example.familiaritywithfragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.familiaritywithfragment.databinding.Fragment1Binding


class Fragment1 : Fragment(R.layout.fragment_1) {

    private var binding: Fragment1Binding? = null
    private var listener: FragmentListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? FragmentListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = Fragment1Binding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {
        binding?.apply {

            btnChangeColor.setOnClickListener {
                listener?.onChangeBackgroundColors()
            }

            btnSwapFragment.setOnClickListener {
                listener?.onSwapFragments()
            }
        }
    }

    interface FragmentListener{
        fun onSwapFragments()

        fun onChangeBackgroundColors()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}